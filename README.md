1. ## Какво причинява северното сияние?

    > Взаимодействието на слънчевия вятър с магнитното поле на Земята.
    >
    > https://www.northernlightscentre.ca/northernlights.html

1. ## Какво е Karman line?

    > Хипотетична граница на височина около 100км, където започва космосът.
    >
    > https://www.nationalgeographic.com/science/2018/12/where-is-the-edge-of-space-and-what-is-the-karman-line/

1. ## Колко хора има в космоса в момента?

    > https://www.howmanypeopleareinspacerightnow.com/

1. ## Kъде отиват сателитите, когато умрат?

    > Ако има достатъчно енергия, го засилват обратно в атмосферата, където изгаря, иначе отива в т.нар. "гробищна орбита".
    >
    > https://spaceplace.nasa.gov/spacecraft-graveyard/en/

1. ## Как се е образувала Луната?

    > Водещата хипотеза е, че Луната се е образувала след сблъсък на младата Земя с обект с размерите на Марс (наречен Тея). Изхвърленият материал се събира под въздействието на гравитацията и формира Луната.
    >
    > https://www.nhm.ac.uk/discover/how-did-the-moon-form.html

1. ## Kолко хора от колко държави са стъпвали на Луната?

    > 12, всички от САЩ
    >
    > https://www.britannica.com/story/how-many-people-have-been-to-the-moon

1. ## Кои планети имат пръстени?

    > Юпитер, Сатурн, Уран, Нептун
    >
    > https://www.universetoday.com/77109/which-planets-have-rings/

1. ## Колко луни има Сатурн?

    > 82, 29 от тях нямат име
    >
    > https://solarsystem.nasa.gov/moons/saturn-moons/overview/?page=0&per_page=40&order=name+asc&search=&placeholder=Enter+moon+name&condition_1=38%3Aparent_id&condition_2=moon%3Abody_type%3Ailike

1. ## Какво е първото име на Уран?

    > Откривателят на Уран го е нарекъл "Планетата на Джордж" или с други думи, Гошо.
    >
    > https://www.mentalfloss.com/article/67483/uranus-used-be-called-schoolyard-friendly-name-george

1. ## Какъв дъжд вали на Венера?

    > Сярна киселина, която никога не достига до повърхността, а се изпарява поради високата температура (> 400C) и след това вали отново.
    >
    > https://blogearth.wordpress.com/2008/02/23/venus-the-planet-where-it-rains-acid/

1. ## Kъде е най-високата планина в Слънчевата система?

    > на Марс, Олимп, 25км.
    >
    > https://www.space.com/20133-olympus-mons-giant-mountain-of-mars.html

1. ## Какъв е цветът на залезите на Марс и защо?

    > Марс има много рядка атмосфера и прашна атмосфера, заради която небето е червено, а залезите сини, обратното на Земята.
    >
    > https://solarsystem.nasa.gov/news/925/what-does-a-sunrise-sunset-look-like-on-mars/

1. ##  Kaкво е Ерис и каква е връзката му с Плутон?

    > Планета-джудже, открита през 2005. Понеже е по-масивна от Плутон, НАСА първоначално обявява Ерис за десетата планета. След това ревизира термина "планета" и намалява бройката на планетите на осем.
    >
    > https://www.universetoday.com/37285/dwarf-planet-eris/

1. ##  [The Milky Way](https://www.space.com/19915-milky-way-galaxy.html)

    ![img](https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/download-1583327893.jpeg)

1. ## Какъв вкус има центърът на Млечния път?

    > На малини. В центъра има огромен облак от съединението етилформат, което придава вкус на малините.
    >
    > https://www.howitworksdaily.com/the-milky-way-smells-of-rum-and-tastes-like-raspberries/

1. ## Kaкво е неутронна звезда?

    > Останки от звезда, толкова плътни, че обект с размерите на кибритена кутийка ще тежи няколко милиарда тона на Земята.
    >
    > https://earthsky.org/astronomy-essentials/definition-what-is-a-neutron-star

1. ## Какво е магнетар?

    > Неутронна звезда с изключително силно магнитно поле; ако жив организъм се приближи на разстояние, по-близко от 1000км, магнитното поле ще откъсне електроните от атомите, които го изграждат и ще го разградят.
    >
    > https://phys.org/news/2016-08-magnetars.html

1. ## Какво е квазар?

    > Ядро на някои галактики, около черните дупки в центъра им се натрупва материал, който при падането си в черната дупка излъчва светлина, която е по-мощна от цялата галактика. 
    >
    > https://www.britannica.com/science/quasar

1. ## Какво е фотонна сфера?

    > Сферична област от пространството, където гравитацията е толкова силна, че кара фотоните да се движат по орбита. С други думи, можете да видите тила си.
    >
    > https://phys.org/news/2014-03-orbit-black-hole.html

1. ##  Какво е White Hole?

    > Част от пространството, в която не може да влиза материя и енергия, а само да излиза. Има теория, че Големия взрив е White Hole, и че тези White holes поставят началото на нови вселени.
    >
    > https://earthsky.org/space/have-we-seen-a-white-hole

1. ## Какво е спагетификация?

    > Когато обект, например човек, приближи област с много силна гравитация (черна дупка), гравитацията, която действа върху краката му е много по-силна от тази в областта на главата и съответно човекът бива разтеглен по вертикала и компресиран по хоризонтала.
    >
    > https://www.phenomena.org/space/spaghettification/

1. ## Какво причинява снежинките в телевизора? 

    > Радиосигнали от космоса, част от тях са остатъчна енергия от the Big Bang, друга част от радиация от Слънцето и други звезди.
    >
    > https://www.ripleys.com/weird-news/static-on-your-tv-is-caused-by-the-big-bang/

1. ## Kaкво са Персеидите и какво ги причинява?

    > Метеоритен дъжд, образува се когато Земята преминава през прах, оставен от опашката на комета.
    >
    > https://earthsky.org/?p=165416

1. ## [Pillars of Creation](https://www.nasa.gov/image-feature/the-pillars-of-creation)

    ![img](https://www.nasa.gov/sites/default/files/thumbnails/image/pillars_of_creation.jpg)

1. ## [The Sombrero Galaxy](https://www.nasa.gov/feature/goddard/2017/messier-104-the-sombrero-galaxy)

    ![img](https://www.constellation-guide.com/wp-content/uploads/2013/08/Sombrero-Galaxy-1024x573.jpg)